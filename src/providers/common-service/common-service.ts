import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastController, Platform } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Diagnostic } from '@ionic-native/diagnostic';
import { Network } from '@ionic-native/network';
import { Subscription } from 'rxjs/Subscription';

@Injectable()
export class CommonServiceProvider {
  liveLatitute: any;
  liveLongitude: any;
  disconnected: Subscription;
  connected: Subscription;
  online: boolean = true;
  constructor(private network: Network, private plt: Platform, private diagnostic: Diagnostic,
    private locationAccuracy: LocationAccuracy, public geolocation: Geolocation, private toastCtrl: ToastController, public http: HttpClient) {
    this.networkInfo();
  }
  toastCreate(msg) {
    this.toastCtrl.create({
      message: msg,
      duration: 3000
    }).present();
  }
  checkLocation() {
    this.plt.ready().then(() => {
      if (this.plt.is('android')) {
        this.diagnostic.isLocationAvailable()
          .then((enabled) => {
            if (enabled) {
              this.getUserPosition();
            }
            else {
              this.enableLocation();
            }
          }).catch(e => {
            console.log(e)
          });
      }
      else {
        this.getUserPosition();
      }
    });
  }
  getUserPosition() {
    let locationOptions = { timeout: 5000, enableHighAccuracy: true };
    this.geolocation.getCurrentPosition(locationOptions).then((resp) => {
      this.liveLatitute = JSON.parse(JSON.stringify(resp.coords.latitude));
      this.liveLongitude = JSON.parse(JSON.stringify(resp.coords.longitude));
    }).catch((error) => {
      this.liveLatitute = 17.3850;
      this.liveLongitude = 78.4867;
      this.toastCreate(error.message);
    });
  }
  enableLocation() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
          () => this.getUserPosition(),
          error => {
            console.log('Error requesting location permissions' + JSON.stringify(error))
          }
        );
      }
    });
  }
  networkInfo() {
    this.connected = this.network.onConnect().subscribe(data => {
      console.log(data);
      this.online = true;
      this.toastCreate('Your Internet Connections appears to be Online');
    });
    this.disconnected = this.network.onDisconnect().subscribe(data => {
      console.log(data);
      this.online = false;
      this.toastCreate('Your Internet Connections appears to be Offline');
    });
  }
}
