import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb';

@Injectable()
export class PouchdbProvider {
  db: any;

  constructor() {
    this.db = new PouchDB('transneuron');
  }
  async getDocsByKey(startKey: string, endKey?: string, descending?: boolean) {
    try {
      endKey = endKey ? endKey : startKey;
      descending = descending ? descending : false;
      let results = await this.db.allDocs({
        include_docs: true,
        startkey: startKey,
        endkey: endKey + '\ufff0',
        descending: descending
      });
      let data = [];
      results.rows.map((row) => {
        data.push(row.doc)
      })
      return data;
    }
    catch (err) {
      return err;
    }
  }
  //to get All documents;
  async getAllDocs() {
    try {
      let results = await this.db.allDocs({ include_docs: true });
      let data = [];
      results.rows.map((row) => {
        data.push(row.doc)
      })
      return data;
    }
    catch (err) {
      return err;
    }
  }
  //to post a document;
  async postDocument(document) {
    try {
      let response = await this.db.post(document);
      return response;
    }
    catch (err) {
      return err;
    }
  }
  //to get a document;
  async getDocument(documentId) {
    try {
      let response = await this.db.get(documentId);
      return response;
    }
    catch (err) {
      return err;
    }
  }
  //to remove a document;
  async removeDocument(document) {
    try {
      let response = await this.db.remove(document);
      return response;
    }
    catch (err) {
      return err;
    }
  }
  //to update a document;
  async updateDocument(document) {
    try {
      let realDoc = await this.getDocument(document._id);
      if (realDoc) {
        document._rev = realDoc._rev;
        let response = await this.db.put(document);
        return response;
      }
      else {
        let response = await this.db.post(document);
        return response;
      }
    }
    catch (err) {
      return err;
    }
  }
  //to post bulk documents;
  async bulkDocuments(docs) {
    try {
      let response = await this.db.bulkDocs(docs);
      return response;
    }
    catch (err) {
      return err;
    }
  }
  //to update bulk documents;
  async updatebulkDocuments(docs) {
    try {

      let response = await this.db.bulkDocs(docs);
      return response;
    }
    catch (err) {
      return err;
    }
  }
  //to delete bulk documents;
  async deletebulkDocuments(docs) {
    try {
      let response = await this.db.bulkDocs(docs);
      return response;
    }
    catch (err) {
      return err;
    }
  }
}
