import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Geolocation } from '@ionic-native/geolocation';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Diagnostic } from '@ionic-native/diagnostic';
import { MyApp } from './app.component';
import { PouchdbProvider } from '../providers/pouch-db/pouch-db';
import { CommonServiceProvider } from '../providers/common-service/common-service';
import { HttpClientModule } from '@angular/common/http';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { Network } from '@ionic-native/network';

@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule, HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar, ScreenOrientation, Network,
    SplashScreen, Geolocation, LocationAccuracy, AndroidPermissions, Diagnostic,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    PouchdbProvider,
    CommonServiceProvider
  ]
})
export class AppModule { }
