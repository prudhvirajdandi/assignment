import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { PouchdbProvider } from '../../providers/pouch-db/pouch-db';
import { CommonServiceProvider } from '../../providers/common-service/common-service';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  signupForm: FormGroup;
  submitData: boolean = false;
  constructor(private service: CommonServiceProvider, private pouchdb: PouchdbProvider, private formBuilder: FormBuilder, public navCtrl: NavController, public navParams: NavParams) {
    this.signupForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.pattern("[a-z0-9!#$%&'*+/=?^-`{l}~-]+(?:\.[a-z0-9!#$%&'*+/=?^-`{l}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9]*[a-z0-9])?")])],
      password: ['', Validators.compose([Validators.minLength(4), Validators.maxLength(20), Validators.required])],
      confirmPassword: ['', Validators.compose([Validators.minLength(4), Validators.maxLength(20), Validators.required])],
      mobileNumber: ['', Validators.compose([Validators.maxLength(10), Validators.minLength(10), Validators.pattern('^(0|[1-9][0-9]*)$'), Validators.required])]
    });
  }
  async registerHandler() {
    this.submitData = true;
    if (this.signupForm.valid) {
      if (this.signupForm.controls['password'].value === this.signupForm.controls['confirmPassword'].value) {
        let key = 'signUp_' + this.signupForm.controls['email'].value;
        let result = await this.pouchdb.getDocument(key);
        if (result.email) {
          this.service.toastCreate('Email already Exist, Try with New Email !');
        }
        else {
          let user = { _id: `signUp_${this.signupForm.controls['email'].value}`, email: this.signupForm.controls['email'].value, password: this.signupForm.controls['password'].value, mobileNumber: this.signupForm.controls['mobileNumber'].value };
          await this.pouchdb.postDocument(user);
          this.service.toastCreate('User Created Successfully,You Can Login.');
          this.navCtrl.setRoot('LoginPage');
        }
      }
      else {
        this.service.toastCreate('Password & Confirm Password Mismatch !');
      }
    }
    else {
      this.service.toastCreate('All the Fields are Required !!');
    }
  }
}
