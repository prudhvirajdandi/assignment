import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { PouchdbProvider } from '../../providers/pouch-db/pouch-db';
import { CommonServiceProvider } from '../../providers/common-service/common-service';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  loginForm: FormGroup;
  submitData: boolean = false;
  usrType = 'C'
  constructor(private service: CommonServiceProvider, private pouchdb: PouchdbProvider, private formBuilder: FormBuilder, public navCtrl: NavController, public navParams: NavParams) {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.pattern("[a-z0-9!#$%&'*+/=?^-`{l}~-]+(?:\.[a-z0-9!#$%&'*+/=?^-`{l}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9]*[a-z0-9])?")])],
      password: ['', Validators.compose([Validators.minLength(4), Validators.maxLength(20), Validators.required])],
      asDelivery: [false]

    });
  }
  asDelivery(event) {
    if (event) {
      this.usrType = "A";
    }
    else {
      this.usrType = "C"
    }
  }
  signUp() {
    this.navCtrl.push('SignupPage');
  }
  forgotPsd() {
    this.navCtrl.push('ForgotPasswordPage');
  }
  async loginHandler() {
    this.submitData = true;
    if (this.loginForm.valid) {
      if (this.service.online) {
        let key = 'signUp_' + this.loginForm.controls['email'].value;
        let result = await this.pouchdb.getDocument(key);
        if (result.password === this.loginForm.controls['password'].value) {
          let split_Data = this.loginForm.controls['email'].value.split('@')
          this.service.toastCreate(`Your are Logged in as ${split_Data[0].toUpperCase()}.`);
          if (this.usrType == "A") {
            this.navCtrl.setRoot('AdminPannelPage', { type: { name: 'Admin-Panel' } });
          }
          else {
            this.navCtrl.setRoot('AdminPannelPage', { type: { name: 'HomePage' } });
          }
        }
        else if (!result.error && result.password !== this.loginForm.controls['password'].value) {
          this.service.toastCreate('Invalid Password, Please Check !');
        }
        if (result.error) {
          this.service.toastCreate("User does't Exist, Please Sign Up !");
        }
      }
      else {
        this.service.toastCreate('Please Connect to Internet !!');
      }

    }
    else {
      this.service.toastCreate('All the Fields are Required !!');
    }
  }
}
