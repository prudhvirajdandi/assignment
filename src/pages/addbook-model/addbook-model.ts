import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { PouchdbProvider } from '../../providers/pouch-db/pouch-db';
import { CommonServiceProvider } from '../../providers/common-service/common-service';


@IonicPage()
@Component({
  selector: 'page-addbook-model',
  templateUrl: 'addbook-model.html',
})
export class AddbookModelPage {
  addbookForm: FormGroup;
  submitData: boolean = false;
  bookId: any;
  image = 'https://cobbhabitatfamilies.files.wordpress.com/2010/02/books.png';
  bookDes = 'Why Your Book Description Matters. The book description is the pitch to the reader about why they should buy your book. It is sales copy to get them to see that the book is for them (or not), and then make the purchase. There are so many examples of book descriptions leading to huge changes in sales.';
  constructor(
    private viewCtrl: ViewController,
    private service: CommonServiceProvider, private pouchdb: PouchdbProvider,
    private formBuilder: FormBuilder, public navCtrl: NavController, public navParams: NavParams) {
    this.addbookForm = this.formBuilder.group({
      bookName: ['', Validators.compose([Validators.maxLength(20), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      bookNumber: ['', Validators.compose([Validators.pattern('^(0|[1-9][0-9]*)$'), Validators.required])],
    });
  }
  ionViewWillEnter() {
    this.service.checkLocation();
    this.bookId = this.navParams.get('bookId');
    console.log(this.bookId);
    if (this.bookId.type == 'Edit') {
      this.addbookForm.controls['bookName'].patchValue(this.bookId.bookName);
      this.addbookForm.controls['bookNumber'].patchValue(this.bookId.bookNumber);
    }
  }
  viewDismiss() {
    this.viewCtrl.dismiss();
  }
  async submitHandler() {
    this.submitData = true;
    let longitude = this.service.liveLongitude;
    let latitute = this.service.liveLatitute;
    if (this.addbookForm.valid) {
      if (longitude && latitute && this.bookId.type == "Add") {
        let postdata = {
          bookName: this.addbookForm.controls['bookName'].value,
          image: this.image,
          bookDes: this.bookDes, lat: latitute, lng: longitude,
          bookNumber: this.addbookForm.controls['bookNumber'].value, _id: `${this.bookId['name'].toLowerCase()}_${Math.floor(Date.now() / 1000)}`,
          addedDate: new Date()
        }
        let response = await this.pouchdb.postDocument(postdata);
        if (response && response.ok == true) {
          this.service.toastCreate('Book Added Successfully..');
          this.viewCtrl.dismiss();
        }
      }
      else if (longitude && latitute && this.bookId.type == "Edit") {
        let postdata = {
          bookName: this.addbookForm.controls['bookName'].value, lat: latitute, lng: longitude,
          bookNumber: this.addbookForm.controls['bookNumber'].value, _id: this.bookId._id,
          addedDate: new Date(),
          image: this.image,
          bookDes: this.bookDes,
        }
        let response = await this.pouchdb.updateDocument(postdata);
        if (response && response.ok == true) {
          this.service.toastCreate('Book Updated Successfully..');
          this.viewCtrl.dismiss();
        }
      }
      else {
        this.service.checkLocation();
      }
    }
    else {
      this.service.toastCreate('All the Fields are Required !!');
    }

  }
}
