import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddbookModelPage } from './addbook-model';

@NgModule({
  declarations: [
    AddbookModelPage,
  ],
  imports: [
    IonicPageModule.forChild(AddbookModelPage),
  ],
})
export class AddbookModelPageModule { }
