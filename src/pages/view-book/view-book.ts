import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-view-book',
  templateUrl: 'view-book.html',
})
export class ViewBookPage {
  bookData: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, ) {
  }

  ionViewWillEnter() {
    this.bookData = this.navParams.get('data');
    console.log(this.bookData);
  }
  viewDismiss() {
    this.viewCtrl.dismiss();
  }

}
