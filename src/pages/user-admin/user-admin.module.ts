import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminPannelPage } from './user-admin';

@NgModule({
  declarations: [
    AdminPannelPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminPannelPage),
  ],
})
export class AdminPannelPageModule { }
