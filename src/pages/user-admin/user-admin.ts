import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-admin-pannel',
  templateUrl: 'user-admin.html',
})
export class AdminPannelPage {
  imageUrl = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR6iiPqwpSOjaPiHjYiHdxdTgaBDdsDA94djaXWEJ5QeTs9Z4AtDA";

  booksList = [
    { name: 'Fiction', image: this.imageUrl, totalBooks: 10 },
    { name: 'Literature', image: this.imageUrl, totalBooks: 20 },
    { name: 'Music', image: this.imageUrl, totalBooks: 30 },
    { name: 'Sci-fi', image: this.imageUrl, totalBooks: 40 }
  ]
  flowData: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  ionViewWillEnter() {
    this.flowData = this.navParams.get('type');
    console.log(this.flowData);
  }
  viewDetails(item) {
    item.appFlow = this.flowData.name;
    this.navCtrl.push('BooksListPage', { 'data': item })
  }
  logout(){
    this.navCtrl.setRoot('LoginPage');
  }
}
