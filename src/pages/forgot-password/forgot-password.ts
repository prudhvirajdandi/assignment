import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { PouchdbProvider } from '../../providers/pouch-db/pouch-db';
import { CommonServiceProvider } from '../../providers/common-service/common-service';

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {
  forgotPswd: FormGroup;
  submitData: boolean = false;
  userResult: any;
  activated: boolean = false;
  constructor(private service: CommonServiceProvider, private pouchdb: PouchdbProvider, private formBuilder: FormBuilder, public navCtrl: NavController, public navParams: NavParams) {
    this.forgotPswd = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.pattern("[a-z0-9!#$%&'*+/=?^-`{l}~-]+(?:\.[a-z0-9!#$%&'*+/=?^-`{l}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9]*[a-z0-9])?")])],
      password: ['', Validators.compose([Validators.minLength(4), Validators.maxLength(20), Validators.required])],
      confirmPassword: ['', Validators.compose([Validators.minLength(4), Validators.maxLength(20), Validators.required])],
    });
  }
  async forgotpsdHandler() {
    let key = 'signUp_' + this.forgotPswd.controls['email'].value;
    let result = await this.pouchdb.getDocument(key);
    if (result.email === this.forgotPswd.controls['email'].value) {
      this.activated = true;
      this.userResult = result;
      console.log(result);
    }
    else if (result.error) {
      this.service.toastCreate('Invalid Email Address..');
    }
  }
  async submitNewPsd() {
    if (this.forgotPswd.controls['password'].value === this.forgotPswd.controls['confirmPassword'].value) {
      let user = { _id: `signUp_${this.forgotPswd.controls['email'].value}`, email: this.forgotPswd.controls['email'].value, password: this.forgotPswd.controls['password'].value };
      await this.pouchdb.updateDocument(user);
      this.service.toastCreate('Password Changed Successfully.,You Can Login.');
      this.navCtrl.setRoot('LoginPage');

    }
    else {
      this.service.toastCreate('Password & Confirm Password Mismatch.');
    }
  }
}
