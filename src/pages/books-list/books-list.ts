import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { PouchdbProvider } from '../../providers/pouch-db/pouch-db';
import { CommonServiceProvider } from '../../providers/common-service/common-service';


@IonicPage()
@Component({
  selector: 'page-books-list',
  templateUrl: 'books-list.html',
})
export class BooksListPage {
  bookdetails: any;
  allBooks = [];
  typeFlow: boolean = false;
  constructor(private service:CommonServiceProvider,private pouchdb: PouchdbProvider, private modalCtrl: ModalController, public navCtrl: NavController, public navParams: NavParams) {
  }

  async ionViewWillEnter() {
    this.bookdetails = this.navParams.get('data');
    console.log(this.bookdetails);
    if (this.bookdetails.appFlow == 'HomePage') {
      this.typeFlow = true;
      console.log('yes');
    }
    let key = this.bookdetails['name'].toLowerCase() + '_';
    this.allBooks = await this.pouchdb.getDocsByKey(key);
  }
  presentModal(item, type) {
    if (item && type == 'Edit') {
      item.name = this.bookdetails.name;
      item.type = 'Edit';
      this.addOrEdit(item);
    }
    else {
      this.bookdetails.type = 'Add';
      this.addOrEdit(this.bookdetails);
    }
  }
  addOrEdit(data) {
    let addBookModal = this.modalCtrl.create('AddbookModelPage', { bookId: data });
    addBookModal.onDidDismiss(data => {
      this.ionViewWillEnter();
    });
    addBookModal.present();
  }
  async delete(item) {
    await this.pouchdb.removeDocument(item);
    this.service.toastCreate('Book Deleted Successfully..');
    this.ionViewWillEnter();
  }
  viewBook(item) {
    if (this.typeFlow) {
      let viewBookModal = this.modalCtrl.create('ViewBookPage', { data: item });
      viewBookModal.present();
    }
  }
}
